# sample-java-app
This is Sample Java Application

###Tools required for development
1. maven 3.6+
2. JDK 8+
3. Git 2.27+
### check tools version using below commands on CMD, GIT Bash or Putty
1. mvn --version
2. git --version
git version: 2.32
3. java -version
Java Version : 1.8.0
###  Going to work on below topics
1. Jenkins
2. Artifactory
3. Selenium
